/*
	VON ARCEO
	s34-Activity
*/

const express = require('express')

const app = express()

const port = 3000

app.use(express.json())

app.use(express.urlencoded({extended: true}))

app.listen(port, () => console.log(`Server is Running at localhost:${port}`))


app.get("/home", (request, response) => {
	response.send('Welcome to the Home Page!')
})

let users = [{
	username: "bongbongmarcos",
	password: "bbm123"
},
{
	username: "lenirobredo",
	password: "leni123"
}]

app.get("/users", (request, response) => {
	response.send(users)
})

app.delete("/delete-users", (request, response) => {
	let message

	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username && request.body.password == users[i].password){
			users.splice(i, 1)
			

			message = `User has been deleted`

			break
		}

		else {
			message = 'User does not exist!'
		}
	}

	response.send(message)
})

